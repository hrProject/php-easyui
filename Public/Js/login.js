$(function() {
    var $dialog = $('<div/>');
    var $formBody = $('#form-body');
    $dialog.dialog({
        height: 300,
        width: 500,
        content: $formBody.show(),
        noheader: true,
        buttons: [{
                id: 'loginBtn',
                disabled: true,
                text: '登陆',
                handler:function(){
                    $.post('public/dologin',$formBody.serialize(),function(rsp){//使用post请求一个页面，rsp表示ajax请求后返回的json字符串
                          if(rsp.status){
                               window.location.reload();
                          }else{
                              $.messager.alert('提示',rsp.msg);
                          }
                    },'JSON').error(function(XMLHttpRequest){
                        $.messager.alert('提示 XMLHttpRequest.status',XMLHttpRequest.status);

                    });
                }
            }]
    });
    $formBody.after($('#logo').show());
    var $verifyInput = $('<input class="verify-input" maxlength="4"/>');
    $verifyInput.keydown(function() {
        return $formBody.form('validate');
    }).keyup(function() {
        var target = this;
        if (target.value.length === 4) {
            target.disabled = true;
            $(target).blur();
            // $.messager.alert('tower_value:',this.value);//by tower
            $.post('public/verifyCode', {
                code: this.value
            }, function(rsp) {
                // $.messager.alert('tower_value:','rsp verifycode');//by tower
                if (rsp.status) {
                    $('#loginBtn').linkbutton('enable').click();
                } else {
                    target.disabled = false;
                    $(target).focus().val('');
                    $verifyImg.click();
                    // $.messager.alert('tower_value:','verifyfail verifycode');//by tower
                }
            }, 'JSON').error(function(XMLHttpRequest) {
                target.disabled = false;
                $(target).focus().val('');
                $verifyImg.click();
                  $.messager.alert("提示 XMLHttpRequest.status",XMLHttpRequest.status);//by tower
            });
        }
    });
    var $verifyImg = $('<img src="public/verify" class="verify-img"/>');
    $verifyImg.on('click', function() {
        $(this).attr('src', 'public/verify?t='+ new Date().getTime());
        //$.messager.alert('tower_imgsrc:',$(this).attr('src'));//by tower
    });
    //$(”元素”).prepend(content); 将content作为该元素的一部分，放到该元素的最前面
    //.dialog-button是easyui的button类名
    $('.dialog-button').prepend($verifyInput).prepend($verifyImg);
    $(window).resize(function() {
        $dialog.dialog('center');
    });
});