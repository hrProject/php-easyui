<?php

/**
 * 作者：GodSon
 * 链接：http://www.btboys.com/
 * 邮箱：wmails@126.com
 */

/**
 * PublicAction
 *
 * @author GodSon
 */
class PublicAction extends Action {

    public function doLogin() {
        //echo 'tower:do login <br />';//注意啊 使用ajax的时候返回数据必须为jason格式，不能随便加echo，不然返回的数据格式就不对
        $type = $_POST['type'];//'type' 是表示登录的是普通用户还是管理员
        $acccount = $_POST['account'];
        $password = $_POST['password'];

        if ($type == 0 && $acccount == C('SYSYTEM_USER_NAME') && $password == C('SYSYTEM_USER_PWD')) {
            session('member', array('uid' => 0, 'uname' => 'SYSTEM'));
            $this->ajaxReturn(array('status' => true));
        } else {
            $Mode = D('User');
            $data = $Mode->field('uid,uname,account,mail')->where(array('account' => $acccount, 'password' => pwdHash($password)))->find();
            if (empty($data)) {
                $this->ajaxReturn(array('status' => false, 'msg' => '用户名或密码错误！'));
            }
            $this->getResourcesByUid($data['uid']);
            session('member', $data);
            $this->ajaxReturn(array('status' => true));
        }
        $this->ajaxReturn(array('status' => false));
    }

    public function doLogout() {
        session(null);
        $this->redirect("/");
    }

    public function verify() {
        //echo "<script language=\"JavaScript\">alert(\"tower:\");</script>";//注意啊 使用ajax的时候返回数据必须为jason格式，不能随便加echo，不然返回的数据格式就不对
        $type = isset($_GET['type']) ? $_GET['type'] : 'gif';
        import("@.ORG.Util.Image");
        Image::buildImageVerify(4, 1, $type);
    }

    public function verifyCode() {
       //echo "<script language=\"JavaScript\">alert(\"tower:\");</script>";
        if (session('verify') != md5($_POST['code'])) {
            $this->ajaxReturn(array('status' => false));
        }
        $this->ajaxReturn(array('status' => true));
    }

    private function getResourcesByUid($uid) {
        $Mode = D('Functions');
        $Resources = $Mode->getResourcesByUid($uid);
        $_allResources = array();
        if (!empty($Resources)) {
            foreach ($Resources as $value) {
                $_resources = $value['resources'];
                if (!empty($_resources)) {
                    $array = explode(';', $_resources);
                    $_allResources = array_merge($_allResources, array_filter($array));
                }
            }
        }
        session('_resources', $_allResources);
    }

}

?>
