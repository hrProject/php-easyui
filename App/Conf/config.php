<?php

return array(
    'URL_MODEL' => '2', //URL模式  
    'SESSION_AUTO_START' => true,
    'SESSION_OPTIONS' => array('expire' => 1800),
    'URL_CASE_INSENSITIVE' => true,
    'DB_TYPE' => 'mysql',
    'DB_HOST' => 'localhost',
    'DB_NAME' => 'btdb',
    'DB_USER' => 'root',
    'DB_PWD' => '',
    'DB_PORT' => '3306',
    'DB_PREFIX' => 'bt_',
    'APP_AUTOLOAD_PATH' => '@.TagLib',
    'SYSYTEM_USER_NAME' => "admin",
    'SYSYTEM_USER_PWD' => '123456',
    'TMPL_PARSE_STRING' => array(
        '__JS__' => '/Public/Js', //JS目录
        '__CSS__' => '/Public/Css', //样式目录
        '__IMG__' => '/Public/Images', //图片目录
        '__THM__' => '/Public/Themes', //主题目录
    ),

     'SHOW_PAGE_TRACE'=>true,//调试时用 by tower 使用trace("add",$name,"Debug",true);看手册
);
// 在APACHE服务器上的访问方式上去除index.php
 
// 下面我说下 apache 下 ，如何 去掉URL 里面的 index.php 
// 例如: 你原来的路径是： localhost/index.php/index 
// 改变后的路径是: localhost/index 

// 1.httpd.conf配置文件中加载了mod_rewrite.so模块 //在APACHE里面去配置 
// #LoadModule rewrite_module modules/mod_rewrite.so把前面的警号去掉 

// 2.在APACHE里面去配置 ，将里面的AllowOverride None都改为AllowOverride All

// 注意：修改之后一定要重启apache服务。 

// 3.确保URL_MODEL设置为2， (url重写模式)

// 在项目的配置文件里写 

// return Array( 
// ‘URL_MODEL’ => ’2′, 
// ); 
// 4 .htaccess文件必须放到跟目录下 

// 这个文件里面加： 


// RewriteEngine on 
// RewriteCond %{REQUEST_FILENAME} !-d 
// RewriteCond %{REQUEST_FILENAME} !-f 
// RewriteRule ^(.*)$ index.php/$1 [L] 
?>